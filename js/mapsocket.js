
    // map initialiation
    var mymap = L.map('mapid').setView([52.53, 13.403], 13);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 18
    }).addTo(mymap);
    //set custom icon for doo2door office
    let customIcon = L.icon({
        iconUrl: 'images/d2d-logo.png',
        iconSize:     [30, 30],
        iconAnchor:   [15, 15],
    });
    let d2d = L.marker([52.53, 13.403], {icon: customIcon}).addTo(mymap);

    // variables to set markers to each individual vehicle
    let vehicles = [];
    let marker = [];

//initiate websocket connection to api server
    let wSocket = new WebSocket("ws://localhost:3000/cable");
    const subscribe = {"command": "subscribe","identifier": "{\"channel\": \"AllygatorChannel\"}"}
    wSocket.onopen = function (event) {
        wSocket.send(JSON.stringify(subscribe));
    };

//listen to websocket broadcast
    wSocket.onmessage = function (event) {
        let jdata = JSON.parse(event.data);
        if (typeof jdata.message === 'object') {
            // log vehicle position
            let pos = jdata.message
            let row = `<tr><td>${pos.vehicle}</td><td>${pos.lat}, ${pos.lng}</td><td>${pos.at}</td></tr>`;
            $('#positions').append(row);

            // set vehicle markers
            if (vehicles.includes(pos.vehicle)) {
                i = vehicles.indexOf(pos.vehicle);
                marker[i].slideTo([pos.lat, pos.lng], {
                    duration: 200,
                    keepAtCenter: false
                });
            } else {
                vehicles.push(pos.vehicle);
                i = vehicles.indexOf(pos.vehicle);
                marker[i] = L.marker([pos.lat, pos.lng]).addTo(mymap);
            }

        }
    }






